require 'test_helper'

class PostTest < ActiveSupport::TestCase
  fixtures :posts

  def test_post_is_valid_with_title_and_body
    post = posts(:one)
    assert post.valid?
  end

  def test_post_is_invalid_without_title_or_body
    post = Post.new
    post.valid?
    assert "can't be blank", post.errors[:title]
    assert "can't be blank", post.errors[:body]
  end
end
