# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Post.delete_all
post = Post.create!(title: "first post", body: "hello world, this is body of the first post")
post.comments.create!([{body: "first comment under the first post"}, {body: "second comment under the first post"}])
