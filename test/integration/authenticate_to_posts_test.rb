require 'test_helper'

class AuthenticateToPostsTest < ActionDispatch::IntegrationTest
  fixtures :all

  def setup
    @post = posts(:one)
  end

  def test_cannot_access_new_and_edit
    get "/posts/#{@post.id}/edit"
    assert_response :unauthorized

    get "/posts/new"
    assert_response :unauthorized
  end

  def test_can_access_index_and_show
    get "/posts"
    assert_equal 200, status

    get "/posts/#{@post.id}"
    assert_equal 200, status
  end

end
