require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  fixtures :comments

  def test_comment_is_valid_with_body_and_post_id
    comment = comments(:one)
    assert comment.valid?
  end

  def test_comment_is_invalid_without_body_or_post_id
    comment = Comment.new
    comment.valid?
    assert "can't be blank", comment.errors[:body]
    assert "can't be blank", comment.errors[:post_id]
  end
end
